# faas-workshops


* install k8s cluster
```
export AWS_PROFILE=...
export DIGITALOCEAN_ACCESS_TOKEN=...

cd terraform/k8s-cluster
terragrunt apply
```


* install faas engines
  - openfaas https://aws.amazon.com/blogs/opensource/deploy-openfaas-aws-eks/
  - kubeless https://kubeless.io/docs/quick-start/
  - nuclio https://nuclio.io/docs/latest/setup/k8s/getting-started-k8s/
  - fission https://docs.fission.io/docs/installation/

```
cd terraform/faas-engine
terragrunt apply
