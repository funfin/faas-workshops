locals {
  k8s_context = "${var.k8s_context}"
}

provider "helm" {
  kubernetes {
    config_context = local.k8s_context
  }
}

provider "kubernetes" {
  load_config_file = true
  version          = "~> 1.11"
  config_path      = var.k8s_kubeconfig
  config_context   = local.k8s_context
}


resource "kubernetes_namespace" "fission" {
  count = var.install ? 1 : 0

  metadata {
    name = "fission"
  }
}


resource "helm_release" "fission-core" {
  count = var.install && var.only_core ? 1 : 0
  name       = "fission"
  chart      = "https://github.com/fission/fission/releases/download/1.11.0/fission-core-1.11.0.tgz "
  namespace  = "fission"

  depends_on = [ kubernetes_namespace.fission ]

}

resource "helm_release" "fission-all" {
  count = var.install && !var.only_core ? 1 : 0
  name       = "fission"
  chart      = "https://github.com/fission/fission/releases/download/1.11.0/fission-all-1.11.0.tgz "
  namespace  = "fission"

  depends_on = [ kubernetes_namespace.fission ]

}

