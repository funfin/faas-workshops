
variable "install" {
  type = bool
}

variable "only_core" {
  type = bool
  default = false
}

variable "k8s_context" {
  type    = string
}

variable "k8s_kubeconfig" {
  type = string
}
