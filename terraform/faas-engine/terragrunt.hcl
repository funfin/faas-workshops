include {
  path = find_in_parent_folders()
}


dependencies {
  paths = ["../k8s-cluster"]
}


dependency "k8s_cluster" {
  config_path = "../k8s-cluster"
}

inputs = {
  k8s_context = dependency.k8s_cluster.outputs.k8s_context
  k8s_kubeconfig = dependency.k8s_cluster.outputs.k8s_kubeconfig
  faas_engines = [
    "openfaas",
    # "nuclio",
    "kubeless",
    "fission",
  ]
  addons = [
    "rabbitmq",
    "external-dns",
    "nginx-ingress",
    "cert-manager",
  ]

  registry_server = "docker.io"
  registry_username = "funfin"

  dns_domain = "do.pkarpik.de"

  # registry_server = "registry.hub.docker.com"
  # registry_username = "funfin"
}