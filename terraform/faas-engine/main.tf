module "openfaas" {
  install                    = contains(var.faas_engines, "openfaas")
  source                     = "./openfaas-module/"
  k8s_context                = var.k8s_context
  k8s_kubeconfig             = var.k8s_kubeconfig
  install_rabbitmq_connector = contains(var.addons, "rabbitmq")
  rabbitmq_password          = module.addons.rabbitmq_password
  dns_domain                 = var.dns_domain
}

module "kubeless" {
  install        = contains(var.faas_engines, "kubeless")
  source         = "./kubeless-module/"
  k8s_context    = var.k8s_context
  k8s_kubeconfig = var.k8s_kubeconfig
}


module "nuclio" {
  install           = contains(var.faas_engines, "nuclio")
  source            = "./nuclio-module/"
  k8s_context       = var.k8s_context
  k8s_kubeconfig    = var.k8s_kubeconfig
  registry_server   = var.registry_server
  registry_username = var.registry_username
  registry_password = var.registry_password
}

module "fission" {
  install        = contains(var.faas_engines, "fission")
  only_core      = false
  source         = "./fission-module/"
  k8s_context    = var.k8s_context
  k8s_kubeconfig = var.k8s_kubeconfig
}

module "addons" {
  source                 = "./addons/"
  install_addons         = var.addons
  k8s_context            = var.k8s_context
  k8s_kubeconfig         = var.k8s_kubeconfig
  dns_domain             = var.dns_domain
  digitalocean_api_token = var.digitalocean_api_token
}