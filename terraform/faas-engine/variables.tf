variable "addons" {
  type = list(string)
}

variable "faas_engines" {
  type = list(string)
}

variable "k8s_context" {
  type = string
}

variable "k8s_kubeconfig" {
  type = string
}

variable "registry_server" {
  type = string
}

variable "registry_username" {
  type = string
}

variable "registry_password" {
  type = string
}

variable "digitalocean_api_token" {
  type = string
}

variable "dns_domain" {
  type = string
}

