
variable "install" {
  type = bool
}

variable "k8s_context" {
  type    = string
}

variable "k8s_kubeconfig" {
  type = string
}

variable "registry_server" {
  type = string
}

variable "registry_username" {
  type = string
}

variable "registry_password" {
  type = string
}
