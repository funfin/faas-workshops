locals {
  k8s_context = "${var.k8s_context}"
}

provider "kubernetes" {
  load_config_file = true
  version          = "~> 1.11"
  config_path      = var.k8s_kubeconfig
  config_context   = local.k8s_context
}


resource "kubernetes_namespace" "nuclio" {
  count = var.install ? 1 : 0

  metadata {
    name = "nuclio"
  }
}


resource "kubernetes_secret" "registry-credentials" {
  count = var.install ? 1 : 0

  metadata {
    name = "registry-credentials"
    namespace  = "nuclio"
  }

  data = {
    ".dockerconfigjson" = <<DOCKER
{
  "auths": {
    "${var.registry_server}": {
      "auth": "${base64encode("${var.registry_username}:${var.registry_password}")}"
    }
  }
}
DOCKER
  }

  type = "kubernetes.io/dockerconfigjson"

  depends_on = [kubernetes_namespace.nuclio]
}



resource "null_resource" "nuclio" {
  count = var.install ? 1 : 0

  triggers = {
    manifest_sha1 = "${sha1("${file("${path.module}/nuclio.yaml")}")}"
    k8s_context = var.k8s_context
  }

  provisioner "local-exec" {
    command = "kubectl --context ${var.k8s_context} apply -f ${path.module}/nuclio.yaml"
  }

  provisioner "local-exec" {
    when = destroy
    command = "kubectl --context ${self.triggers.k8s_context} delete -f ${path.module}/nuclio.yaml"
  }

  depends_on = [kubernetes_namespace.nuclio, kubernetes_secret.registry-credentials]
}