locals {
  k8s_context = "${var.k8s_context}"
}

provider "helm" {
  kubernetes {
    config_context = local.k8s_context
  }
}

provider "kubernetes" {
  load_config_file = true
  version          = "~> 1.11"
  config_path      = var.k8s_kubeconfig
  config_context   = local.k8s_context
}


resource "kubernetes_namespace" "rabbitmq" {
  count = contains(var.install_addons, "rabbitmq") ? 1 : 0
  metadata {
    name = "rabbitmq"
  }
}


resource "random_string" "rabbit-password" {
  count = contains(var.install_addons, "rabbitmq") ? 1 : 0
  length  = 12
  special = false
  upper   = true
}



resource "kubernetes_secret" "rabbitmq-secret" {
  count = contains(var.install_addons, "rabbitmq") ? 1 : 0
  metadata {
    name      = "rabbitmq-secret"
    namespace = "rabbitmq"
  }

  data = {
    "rabbitmq-password"     = "${random_string.rabbit-password[0].result}"
    "rabbitmq-erlang-cookie" = "WnpIAtXpWQI8RFpK2J=UGE1iothIFUYCpbqVw3McO2BH"
  }

  depends_on = [kubernetes_namespace.rabbitmq]
}


resource "helm_release" "this" {
  count = contains(var.install_addons, "rabbitmq") ? 1 : 0
  name       = "rabbitmq"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "rabbitmq"
  version    = "7.6.6"
  namespace  = "rabbitmq"

  set {
    type  = "string"
    name  = "auth.username"
    value = "rabbitmq"
  }

  set {
    type  = "string"
    name  = "auth.existingPasswordSecret"
    value = "rabbitmq-secret"
  }

  set {
    type  = "string"
    name  = "auth.existingErlangSecret"
    value = "rabbitmq-secret"
  }


  depends_on = [ kubernetes_secret.rabbitmq-secret ]

}
resource "helm_release" "nginx-ingress" {
  count      = contains(var.install_addons, "nginx-ingress") ? 1 : 0
  name       = "nginx-ingress"
  namespace  = "kube-system"
  # repository = "https://helm.nginx.com/stable"
  repository = "https://kubernetes-charts.storage.googleapis.com"
  chart      = "nginx-ingress"
  # version    = "0.6.1"
  version    = "1.37.0"

  set {
    type  = "string"
    name  = "controller.publishService.enabled"
    value = "true"
  }
}

resource "random_string" "suffix" {
  length  = 4
  special = false
  upper   = false
}


resource "helm_release" "external-dns" {
  count      = contains(var.install_addons, "external-dns") ? 1 : 0
  name       = "external-dns-${random_string.suffix.result}"
  namespace  = "external-dns"
  create_namespace = true
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "external-dns"
  version    = "3.3.0"

  set {
    type  = "string"
    name  = "provider"
    value = "digitalocean"
  }
  set {
    type  = "string"
    name  = "digitalocean.apiToken"
    value = var.digitalocean_api_token
  }
  set {
    type  = "string"
    name  = "interval"
    value = "1m"
  }

  set {
    type  = "string"
    name  = "policy"
    value = "sync"
    # value = "upsert-only"
  }

  set {
    type  = "string"
    name  = "domainFilters[0]"
    value = var.dns_domain
  }

  set {
    type  = "string"
    name  = "rbac.create"
    value = "true"
  }


}


resource "kubernetes_namespace" "cert-manager" {
  count      = contains(var.install_addons, "cert-manager") ? 1 : 0

  metadata {
    name = "cert-manager"
  }
}


resource "null_resource" "cert-manager-crd" {
  count      = contains(var.install_addons, "cert-manager") ? 1 : 0

  triggers = {
    manifest_sha1 = "${sha1("${file("${path.module}/cert-manager.crds.yaml")}")}"
    k8s_context = var.k8s_context
  }

  provisioner "local-exec" {
    command = "kubectl --context ${var.k8s_context} apply -f ${path.module}/cert-manager.crds.yaml"
  }

  provisioner "local-exec" {
    when = destroy
    command = "kubectl --context ${self.triggers.k8s_context} delete -f ${path.module}/cert-manager.crds.yaml"
  }

  depends_on = [kubernetes_namespace.cert-manager]
}



resource "helm_release" "cert-manager" {
  count      = contains(var.install_addons, "cert-manager") ? 1 : 0
  name       = "cert-manager"
  namespace  = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "v0.14.1"  # same version as crd

  depends_on = [
    kubernetes_namespace.cert-manager,
    null_resource.cert-manager-crd
  ]
}


resource "null_resource" "cert-manager-clusterissuer" {
  count      = contains(var.install_addons, "cert-manager") ? 1 : 0

  triggers = {
    manifest_sha1 = "${sha1("${file("${path.module}/clusterissuers.yaml")}")}"
    k8s_context = var.k8s_context
  }

  provisioner "local-exec" {
    command = "kubectl --context ${var.k8s_context} apply -f ${path.module}/clusterissuers.yaml"
  }

  provisioner "local-exec" {
    when = destroy
    command = "kubectl --context ${self.triggers.k8s_context} delete -f ${path.module}/clusterissuers.yaml"
  }

  depends_on = [
    kubernetes_namespace.cert-manager,
    null_resource.cert-manager-crd,
    helm_release.cert-manager
  ]
}