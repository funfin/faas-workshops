
variable "install_addons" {
  type = list(string)
}

variable "k8s_context" {
  type    = string
}

variable "k8s_kubeconfig" {
  type = string
}

variable "digitalocean_api_token" {
  type = string
}

variable "dns_domain" {
  type = string
}