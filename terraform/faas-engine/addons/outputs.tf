output rabbitmq_password {
  value = length(random_string.rabbit-password) > 0 ? random_string.rabbit-password[0].result : ""
}

