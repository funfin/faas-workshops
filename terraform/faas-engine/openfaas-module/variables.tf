
variable "install" {
  type = bool
}

variable "install_rabbitmq_connector" {
  type = bool
}

variable "k8s_context" {
  type    = string
}

variable "k8s_kubeconfig" {
  type = string
}

variable "rabbitmq_password" {
  type = string
}

variable "dns_domain" {
  type = string
}