locals {
  k8s_context = "${var.k8s_context}"
}

provider "helm" {
  kubernetes {
    config_context = local.k8s_context
  }
}

provider "kubernetes" {
  load_config_file = true
  version          = "~> 1.11"
  config_path      = var.k8s_kubeconfig
  config_context   = local.k8s_context
}



resource "null_resource" "namespaces" {
  count = var.install ? 1 : 0

  triggers = {
    manifest_sha1 = "${sha1("${file("${path.module}/namespaces.yaml")}")}"
    k8s_context = var.k8s_context
  }

  provisioner "local-exec" {
    command = "kubectl --context ${var.k8s_context} apply -f ${path.module}/namespaces.yaml"
  }

  provisioner "local-exec" {
    when = destroy
    command = "kubectl --context ${self.triggers.k8s_context} delete -f ${path.module}/namespaces.yaml"
  }
}



resource "random_string" "password" {
  count = var.install ? 1 : 0

  length  = 15
  special = true
  upper   = true
}



resource "kubernetes_secret" "basicauth" {
  count = var.install ? 1 : 0
  metadata {
    name      = "basic-auth"
    namespace = "openfaas"
  }

  data = {
    "basic-auth-user"     = "admin"
    "basic-auth-password" = "${random_string.password[0].result}"
  }

  depends_on = [null_resource.namespaces]
}


resource "helm_release" "this" {
  count = var.install ? 1 : 0
  name       = "openfass"
  repository = "https://openfaas.github.io/faas-netes/"
  chart      = "openfaas"
  version    = "6.0.3"
  namespace  = "openfaas"

  set {
    type  = "string"
    name  = "functionNamespace"
    value = "openfaas-fn"
  }

  set {
    type  = "string"
    name  = "serviceType"
    value = "LoadBalancer"
  }

  set {
    type  = "string"
    name  = "basic_auth"
    value = "true"
  }

  set {
    type  = "auto"
    name  = "operator.create"
    value = "true"
  }

  set {
    type  = "string"
    name  = "gateway.replicas"
    value = "2"
  }

  set {
    type  = "string"
    name  = "queueWorker.replicas"
    value = "2"
  }

  set {
    type = "string"
    name = "exposeServices"
    value = "false"
  }

  set {
    type = "string"
    name = "ingress.enabled"
    value = "true"
  }

  set {
    type = "string"
    name = "ingress.hosts[0].host"
    value = "openfaas.${var.dns_domain}"
  }

  set {
    type = "string"
    name = "ingress.hosts[0].serviceName"
    value = "gateway"
  }

  set {
    type = "string"
    name = "ingress.hosts[0].servicePort"
    value = "8080"
  }
  set {
    type = "string"
    name = "ingress.hosts[0].path"
    value = "/"
  }

  depends_on = [ kubernetes_secret.basicauth ]

}


data "template_file" "rabbitmq-connector-config" {
    template = "${file("${path.module}/rabbitmq-connector-config.yaml")}"

    vars = {
        rabbitmq_password = var.rabbitmq_password
    }
}

resource "null_resource" "rabbitmq-connector-config" {
  count = var.install && var.install_rabbitmq_connector ? 1 : 0
  triggers = {
    manifest_sha1 = "${sha1("${data.template_file.rabbitmq-connector-config.rendered}")}"
  }

  provisioner "local-exec" {
    command = "kubectl --context ${var.k8s_context} apply -f -<<EOF\n${data.template_file.rabbitmq-connector-config.rendered}\nEOF"
  }
}


data "template_file" "rabbitmq-connector-deployment" {
    template = "${file("${path.module}/rabbitmq-connector-deployment.yaml")}"
}

resource "null_resource" "rabbitmq-connector-deployment" {
  count = var.install && var.install_rabbitmq_connector ? 1 : 0

  triggers = {
    manifest_sha1 = "${sha1("${data.template_file.rabbitmq-connector-deployment.rendered}")}"
  }

  provisioner "local-exec" {
    command = "kubectl --context ${var.k8s_context} apply -f -<<EOF\n${data.template_file.rabbitmq-connector-deployment.rendered}\nEOF"
  }
}