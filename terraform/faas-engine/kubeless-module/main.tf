locals {
  k8s_context = "${var.k8s_context}"
}

provider "kubernetes" {
  load_config_file = true
  version          = "~> 1.11"
  config_path      = var.k8s_kubeconfig
  config_context   = local.k8s_context
}


resource "kubernetes_namespace" "kubeless" {
  count = var.install ? 1 : 0

  metadata {
    name = "kubeless"
  }
}


resource "null_resource" "kubeless" {
  count = var.install ? 1 : 0

  triggers = {
    manifest_sha1 = "${sha1("${file("${path.module}/kubeless.yaml")}")}"
    k8s_context = var.k8s_context
  }

  provisioner "local-exec" {
    command = "kubectl --context ${var.k8s_context} apply -f ${path.module}/kubeless.yaml"
  }

  provisioner "local-exec" {
    when = destroy
    command = "kubectl --context ${self.triggers.k8s_context} delete -f ${path.module}/kubeless.yaml"
  }

  depends_on = [kubernetes_namespace.kubeless]
}
