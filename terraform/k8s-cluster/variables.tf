variable "cluster_name" {
  type = string
}

variable "k8s_kubeconfig" {
  type = string
}