module "k8s-cluster" {
  source            = "../../terraform-k8s-clusters/providers/do"
  worker_node_count = 1
  worker_node_type  = "s-2vcpu-4gb"
  cluster_name      = var.cluster_name
  k8s_version       = "1.18"
  region            = "fra1"
  k8s_kubeconfig    = pathexpand(var.k8s_kubeconfig)
}


