include {
  path = find_in_parent_folders()
}


inputs = {
  cluster_name = "do-fra1-1"
  k8s_kubeconfig = "~/.kube/config"
}