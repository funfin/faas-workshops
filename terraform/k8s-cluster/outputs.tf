output k8s_context {
  value = module.k8s-cluster.k8s_context
}


output "k8s_kubeconfig" {
  value = var.k8s_kubeconfig
}