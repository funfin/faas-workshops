remote_state {
  backend = "s3"
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    bucket = "faas-workshops-terraform"

    key     = "${path_relative_to_include()}/terraform.tfstate"
    region  = "eu-west-1"
    profile = "funfin"
    encrypt = true
    dynamodb_table = "faas-workshops-terraform-lock-table"
  }
}