package pl.udb;

import io.kubeless.Event;
import io.kubeless.Context;

import org.junit.Assert;
import org.junit.Test;

public class FormTest {

	@Test
	public void testResponse() {
		Form hw = new Form();
		String resp = hw.call(null, null);
		Assert.assertTrue( resp.contains("/java-eval"));
	}
}