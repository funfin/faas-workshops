
```
def hello(event, context):
  print event
  #return event['data']
  return "hello world"


$ kubeless function deploy hello --runtime python2.7 \
                                --from-file test.py \
                                --handler test.hello




$ kubeless function ls
# !
$ kubectl get functions.kubeless.io

$ kubeless function call hello

# $ kubeless trigger http create hello --function-name hello --path hello --hostname kubeless.aleo.udb
$ kubectl apply -f hello-http-trtigger.yaml

$ curl http://kubeless.aleo.udb/hello


cd java/01

kubeless function deploy get-java-deps --runtime java1.8 --handler Hello.sayHello --from-file HelloWithDeps.java --dependencies pom.xml

kubeless function call get-java-deps


cd java/02/form
$ ./build.sh
$ kubeless function deploy form --runtime jvm1.8 --from-file target/function-1.0-SNAPSHOT-jar-with-dependencies.jar --handler pl_udb_Form.call
$ kubectl apply -f form-http-trtigger.yaml
$ open http://kubeless.aleo.udb/hello
```
