$ faas-cli new --lang python3 astronaut-finder --prefix=funfin
$ faas-cli build -f ./astronaut-finder.yml
$ faas-cli deploy -f ./astronaut-finder.yml
$ echo | faas-cli invoke astronaut-finder
$ kubectl -n openfaas-fn logs -f astronaut-finder-......

