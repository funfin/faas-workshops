faas-cli new --lang java8 hello-java8-openfaas --prefix=funfin
faas-cli new --lang java8 hello-java11-openfaas --prefix=funfin


faas-cli up -f hello-java8-openfaas.yml
faas-cli up -f hello-java11-openfaas.yml