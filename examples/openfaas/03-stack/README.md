* Managing multiple functions
```
$ faas-cli new --lang python3 first
$ faas-cli new --lang python3 second --append=./first.yml

$ mv first.yml example.yml

$ faas-cli build -f ./example.yml --parallel=2

```
* To run faas-cli build && faas-cli push && faas-cli deploy together, use faas-cli up
* Pro-tip: stack.yml is the default name the faas-cli will look for if you don't want to pass a -f parameter

```
cp example.yml stack.yml
$ faas-cli up
```