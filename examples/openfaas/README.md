* test openfaas
```
export OPENFAAS_IP=$(kubectl get svc -n openfaas gateway-external -o  jsonpath='{.status.loadBalancer.ingress[*].ip}')

doctl compute domain records create do.pkarpik.de --record-name openfaas --record-type A --record-ttl 300 --record-data $OPENFAAS_IP

export OPENFAAS_URL="openfaas.do.pkarpik.de:8080"
export OPENFAAS_URL="$OPENFAAS_IP:8080"
echo Your gateway URL is: $OPENFAAS_URL


echo $(kubectl -n openfaas get  secret basic-auth  -o=jsonpath='{.data.basic-auth-password}' | base64 -d) | faas-cli login --username admin -g http://openfaas.do.pkarpik.de:8080 --password-stdin

echo $(kubectl -n openfaas get  secret basic-auth  -o=jsonpath='{.data.basic-auth-password}' | base64 -d) | faas-cli login --username admin --password-stdin


faas-cli store deploy inception

curl -i $OPENFAAS_URL/function/inception --data https://upload.wikimedia.org/wikipedia/commons/7/79/2010-brown-bear.jpg

curl -i $OPENFAAS_URL/function/inception --data "Hi UDB"


# List available languages
faas-cli new --list
# Downloads templates from the specified git repo
$ faas-cli template pull


cd 01


