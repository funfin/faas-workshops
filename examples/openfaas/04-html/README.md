* HTML example

```
$ faas-cli new --lang python3 show-html --prefix=$DOCKER_REGISTRY

$ faas-cli up -f show-html.yml

$ faas-cli describe -f show-html.yml show-html

$ open http://openfaas.do.pkarpik.de:8080/function/show-html?action=new
$ open http://openfaas.do.pkarpik.de:8080/function/show-html?action=list


$ faas-cli store deploy figlet

$ curl http://openfaas.do.pkarpik.de:8080/function/figlet -d "test"

$ open http://openfaas.do.pkarpik.de:8080/function/show-html?action=figlet
```