package io.fission;

import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

public class Eval implements Function {

	@Override
	public ResponseEntity<?> call(RequestEntity req, Context context) {

		try {
			Map<String, String> queryParameters = getQueryParameters(req);
			System.out.println( "\n\n test \n\n "+ queryParameters);

			int num_1 = Integer.parseInt(queryParameters.get("num_1"));
			int num_2 = Integer.parseInt(queryParameters.get("num_2"));
			String operator = queryParameters.get("operator");
			Integer result = null;
			switch (operator) {
				case "*":
					result = num_1 * num_2;
					break;
				case "-":
					result = num_1 - num_2;
					break;
				default:
					return (ResponseEntity<?>) ResponseEntity.ok("supported operators: *, -");
			}
			return ResponseEntity.ok(num_1+ " "+operator+" "+num_2+" = "+result);
		} catch (UnsupportedEncodingException e) {
			return (ResponseEntity<?>) ResponseEntity.status(501);
		}
	}

	private Map<String, String> getQueryParameters(RequestEntity req) throws UnsupportedEncodingException {
		Map<String, String> queryPaurs = new LinkedHashMap<String, String>();
  		URI url = req.getUrl();
		String query = url.getQuery();
		String[] pairs = query.split("&");
		for (String pair : pairs) {
			int idx = pair.indexOf("=");
			queryPaurs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
		}
		return queryPaurs;
	}

}