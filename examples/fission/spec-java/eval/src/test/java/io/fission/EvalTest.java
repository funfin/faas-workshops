package io.fission;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.net.URISyntaxException;

public class EvalTest {

	@Test
	public void testMultiplication() {
		Eval hw = new Eval();
		int num1=2;
		int num2=3;
		String operator = "*";

		ResponseEntity resp = hw.call(buildRequest(num1, num2, operator), null);

		Assert.assertEquals("2 * 3 = 6", resp.getBody().toString());
	}

	@Test
	public void testSubtraction() {
		Eval hw = new Eval();
		int num1=2;
		int num2=3;
		String operator = "-";

		ResponseEntity resp = hw.call(buildRequest(num1, num2, operator), null);

		Assert.assertEquals("2 - 3 = -1", resp.getBody().toString());
	}


	private RequestEntity buildRequest(int num1, int num2, String operator) {
		RequestEntity request = null;
		try {
			request = RequestEntity.get(new URI("http://example.com/java-eval?num_1="+num1+"&num_2="+num2+"&operator="+operator+"&end=true")).build();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return request;
	}

}