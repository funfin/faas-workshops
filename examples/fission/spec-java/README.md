```
$ fission spec init

$ fission env create --spec --name java --image fission/jvm-env --builder fission/jvm-builder --keeparchive --version 2 --poolsize=1

$ fission package create --spec --name java-form-pkg --env java --src "form/*"
$ fission package create --spec --name java-eval-pkg --env java --src "eval/*"

$ fission fn create --spec --name java-form --pkg  java-form-pkg --env java --entrypoint io.fission.Form --executortype poolmgr
$ fission fn create --spec --name java-eval --pkg  java-eval-pkg --env java --entrypoint io.fission.Eval --executortype poolmgr

# or ty executor newdeploy with min max scale
# $ fission fn create --spec --name java-form --pkg  java-src-pkg-zip-h6ge --env java --entrypoint io.fission.HelloWorld --executortype newdeploy --minscale 1 --maxscale 1

$ fission route create --spec --method GET --url /java-form --function java-form
$ fission route create --spec --method GET --url /java-eval --function java-eval

$ fission spec apply --wait
```