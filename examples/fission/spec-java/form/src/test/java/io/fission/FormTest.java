package io.fission;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.net.URISyntaxException;

public class FormTest {

	@Test
	public void testResponse() {
		Form hw = new Form();
		RequestEntity request = null;
		try {
			request = RequestEntity.get(new URI("http://example.com/bar")).build();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		ResponseEntity resp = hw.call(request, null);
		Assert.assertTrue( resp.getBody().toString().contains("/java-eval"));
	}
}