package io.fission;

import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import io.fission.Function;
import io.fission.Context;

public class Form implements Function {

	@Override
	public ResponseEntity<?> call(RequestEntity req, Context context) {
		return ResponseEntity.ok("<html><body><form action=\"/java-eval\" method=\"GET\">Number 1 : <input name=\"num_1\"/><br>Number 2: <input name=\"num_2\"/><br>Operator: <input name=\"operator\"/><input type=\"submit\" value=\"submit\"></form></body></html>");
	}

}