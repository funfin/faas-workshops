* https://docs.fission.io/docs/spec/


```
$ fission spec init

$ fission env create --spec --name python --image fission/python-env --builder fission/python-builder --poolsize=1

$ fission function create --spec --name calc-form --env python --src "form/*" --entrypoint form.main
$ fission function create --spec --name calc-eval --env python --src "eval/*" --entrypoint eval.main

$ fission route create --spec --method GET --url /form --function calc-form
$ fission route create --spec --method GET --url /eval --function calc-eval

$ fission spec validate
$ fission spec apply --wait

$ export FISSION_ROUTER=$(kubectl --namespace fission get svc router -o jsonpath='{.status.loadBalancer.ingress[*].ip}')

$ open http://$FISSION_ROUTER/form
```